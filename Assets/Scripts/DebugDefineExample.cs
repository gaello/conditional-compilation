﻿#define EXAMPLE_DEBUG // <- My custom define, works only within this file.
using UnityEngine;

/// <summary>
/// This script show how you can use conditional compilation in development.
/// By commenting out the first line you are disabling all Debug Logs from this script.
/// </summary>
public class DebugDefineExample : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
#if EXAMPLE_DEBUG
        Debug.LogFormat("{0} TriggerEnter by {1}", gameObject, other.gameObject);
#endif
    }

    private void OnTriggerStay(Collider other)
    {
#if EXAMPLE_DEBUG
        Debug.LogFormat("{0} TriggerStay by {1}", gameObject, other.gameObject);
#endif
    }

    private void OnTriggerExit(Collider other)
    {
#if EXAMPLE_DEBUG
        Debug.LogFormat("{0} TriggerExit by {1}", gameObject, other.gameObject);
#endif
    }
}
