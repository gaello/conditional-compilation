﻿using UnityEngine;

/// <summary>
/// This script show difference between if-statement and conditional compilation.
/// </summary>
public class DifferenceExample : MonoBehaviour
{
    /// <summary>
    /// Unity method called on first frame.
    /// </summary>
    void Start()
    {
        // Example built with regual if-statement
        if (Application.platform == RuntimePlatform.OSXEditor
            || Application.platform == RuntimePlatform.WindowsEditor
            || Application.platform == RuntimePlatform.LinuxEditor)
        {
            Debug.Log("You are running this code in the editor.");
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            Debug.Log("This is Android platform.");
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Debug.Log("This is iOS platform.");
        }


        // Example built with conditional compilation
#if UNITY_EDITOR
        Debug.Log("You are running this code in the editor.");
#endif

#if UNITY_ANDROID
        Debug.Log("This is Android platform.");
#elif UNITY_IOS
        Debug.Log("This is iOS platform.");
#endif

    }
}
